/**
 * 
 */
package com.example.mybatis.Dto;

import java.util.List;

/**
 * @author Avinash
 *
 */
public class BlogDto {

	public Integer blogId;

	public String blogName;
	
	public List<PostDto> posts;

	/**
	 * @return the blogId
	 */
	public Integer getBlogId() {
		return blogId;
	}

	/**
	 * @param blogId the blogId to set
	 */
	public void setBlogId(Integer blogId) {
		this.blogId = blogId;
	}

	/**
	 * @return the blogName
	 */
	public String getBlogName() {
		return blogName;
	}

	/**
	 * @param blogName the blogName to set
	 */
	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}

	/**
	 * @return the posts
	 */
	public List<PostDto> getPosts() {
		return posts;
	}

	/**
	 * @param posts the posts to set
	 */
	public void setPosts(List<PostDto> posts) {
		this.posts = posts;
	}

}
