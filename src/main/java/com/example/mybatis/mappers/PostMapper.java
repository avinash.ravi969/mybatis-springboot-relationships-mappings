/**
 * 
 */
package com.example.mybatis.mappers;

import java.util.List;

import com.example.mybatis.domain.Comment;
import com.example.mybatis.domain.CommentPost;
import com.example.mybatis.domain.Post;


/**
 * @author Avinash
 *
 */
public interface PostMapper {
	
	public Post getPostById(Integer postId);

	public List<Post> getAllPosts();

	public void insertPost(Post post);
	
	public void insertComment(Comment comment);
	
	public void insertCommentPost(CommentPost commonPost);
	
	public List<Post> getPostsByBlogId(Integer blogId);
}
