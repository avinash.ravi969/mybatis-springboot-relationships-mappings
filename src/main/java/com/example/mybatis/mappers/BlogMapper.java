/**
 * 
 */
package com.example.mybatis.mappers;

import java.util.List;

import com.example.mybatis.domain.Blog;

/**
 * @author Avinash
 *
 */
public interface BlogMapper {

	public Blog getBlogById(Integer blogId);

	public List<Blog> getAllBlogs();

	public void insertBlog(Blog blog);
}
