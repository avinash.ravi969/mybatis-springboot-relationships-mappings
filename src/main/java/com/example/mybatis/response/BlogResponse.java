/**
 * 
 */
package com.example.mybatis.response;

import java.util.List;

import com.example.mybatis.Dto.BlogDto;

/**
 * @author Avinash
 *
 */
public class BlogResponse {
	
	public List<BlogDto> blogs;

	/**
	 * @return the blogs
	 */
	public List<BlogDto> getBlogs() {
		return blogs;
	}

	/**
	 * @param blogs the blogs to set
	 */
	public void setBlogs(List<BlogDto> blogs) {
		this.blogs = blogs;
	}
	
	
}
