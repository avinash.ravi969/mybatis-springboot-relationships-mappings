package com.example.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.mybatis.Dto.UserDto;
import com.example.mybatis.domain.User;
import com.example.mybatis.mappers.UserMapper;

/**
 * 
 * @author Avinash
 *
 */
@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	UserMapper userMapper;
	
	@RequestMapping(value="/user", method= RequestMethod.POST)
	public ResponseEntity<UserDto> createUser(@RequestBody User user) {
		userMapper.insertUser(user);
		UserDto userDto = new UserDto();
		userDto.setEmailId(user.getEmailId());
		return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);
	}
}
