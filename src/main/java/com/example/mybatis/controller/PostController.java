/**
 * 
 */
package com.example.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.mybatis.Dto.PostDto;
import com.example.mybatis.domain.Comment;
import com.example.mybatis.domain.CommentPost;
import com.example.mybatis.domain.Post;
import com.example.mybatis.mappers.PostMapper;

/**
 * @author Avinash
 *
 */
@RestController
@RequestMapping("/api")
public class PostController {
	
	@Autowired
	PostMapper postMapper;
	
	@RequestMapping(value="/post", method=RequestMethod.POST)
	public ResponseEntity<PostDto> createPost(@RequestBody Post post) {
		postMapper.insertPost(post);
		
		for (Comment comment : post.getComments()) {
			postMapper.insertComment(comment);
			
			CommentPost commentPost = new CommentPost();
			commentPost.setCommentId(comment.getCommentId());
			commentPost.setPostId(post.getPostId());
			
			postMapper.insertCommentPost(commentPost);
		}
		
		PostDto postDto = new PostDto();
		postDto.setPostId(post.getPostId());
		return new ResponseEntity<PostDto>(postDto, HttpStatus.OK);
	}
}
