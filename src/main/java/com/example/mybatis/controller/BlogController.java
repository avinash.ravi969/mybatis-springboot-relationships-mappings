/**
 * 
 */
package com.example.mybatis.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.mybatis.Dto.BlogDto;
import com.example.mybatis.Dto.PostDto;
import com.example.mybatis.domain.Blog;
import com.example.mybatis.domain.Post;
import com.example.mybatis.mappers.BlogMapper;
import com.example.mybatis.mappers.PostMapper;
import com.example.mybatis.response.BlogResponse;

/**
 * @author Avinash
 *
 */
@RestController
@RequestMapping("/api")
public class BlogController {
	
	@Autowired
	BlogMapper blogMapper;
	
	@Autowired
	PostMapper postMapper;
	
	@RequestMapping(value="/blog", method= RequestMethod.POST)
	public ResponseEntity<BlogDto> createBlog(@RequestBody Blog blog) {
		blogMapper.insertBlog(blog);
		BlogDto blogDto = new BlogDto();
		blogDto.setBlogName(blog.getBlogName());
		return new ResponseEntity<BlogDto>(blogDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/blog", method=RequestMethod.GET)
	public ResponseEntity<BlogResponse> getAllBlogs() {
		BlogResponse response = new BlogResponse();
		List<BlogDto> blogs = new ArrayList<>();
		List<Blog> blogDtos = blogMapper.getAllBlogs();
		
		for(Blog blog : blogDtos) {
			BlogDto blogDto = new BlogDto();
			blogDto.setBlogId(blog.getBlogId());
			blogDto.setBlogName(blog.getBlogName());
			
			
			List<Post> posts = postMapper.getPostsByBlogId(blog.getBlogId());
			if (!posts.isEmpty()) {
				List<PostDto> postsDetails = new ArrayList<>();
				for (Post post : posts) {
					PostDto postDto = new PostDto();
					postDto.setBlogId(post.getBlogId());
					postDto.setPostId(post.getPostId());
					postDto.setContent(post.getContent());
					postDto.setTitle(post.getTitle());
					
					postsDetails.add(postDto);
				}
				blogDto.setPosts(postsDetails);
			}
			
			blogs.add(blogDto);
		}
		response.setBlogs(blogs);
		return new ResponseEntity<BlogResponse>(response, HttpStatus.OK);
	}
}
